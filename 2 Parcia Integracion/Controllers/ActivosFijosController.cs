﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _2_Parcia_Integracion.Data;
using _2_Parcia_Integracion.Models;
using Microsoft.AspNetCore.Authorization;

namespace _2_Parcia_Integracion.Controllers
{
    [Authorize]
    public class ActivosFijosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ActivosFijosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ActivosFijos
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ActivosFijos.Include(a => a.Departamento).Include(a => a.Tipo_Activo);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ActivosFijos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activosFijos = await _context.ActivosFijos
                .Include(a => a.Departamento)
                .Include(a => a.Tipo_Activo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (activosFijos == null)
            {
                return NotFound();
            }

            return View(activosFijos);
        }

        // GET: ActivosFijos/Create
        public IActionResult Create()
        {
            ViewData["DepartamentoID"] = new SelectList(_context.Set<Departamento>(), "Id", "Descripcion");
            ViewData["Tipo_ActivoID"] = new SelectList(_context.Set<TipoDeActivo>(), "Id", "Descripcion");
            return View();
        }

        // POST: ActivosFijos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Descripcion,DepartamentoID,Tipo_ActivoID,Fecha_Registro,Valor_Compra,Depreciacion_Acumulada")] ActivosFijos activosFijos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(activosFijos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartamentoID"] = new SelectList(_context.Set<Departamento>(), "Id", "Descripcion", activosFijos.DepartamentoID);
            ViewData["Tipo_ActivoID"] = new SelectList(_context.Set<TipoDeActivo>(), "Id", "Descripcion", activosFijos.Tipo_ActivoID);
            return View(activosFijos);
        }

        // GET: ActivosFijos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activosFijos = await _context.ActivosFijos.FindAsync(id);
            if (activosFijos == null)
            {
                return NotFound();
            }
            ViewData["DepartamentoID"] = new SelectList(_context.Set<Departamento>(), "Id", "Descripcion", activosFijos.DepartamentoID);
            ViewData["Tipo_ActivoID"] = new SelectList(_context.Set<TipoDeActivo>(), "Id", "Descripcion", activosFijos.Tipo_ActivoID);
            return View(activosFijos);
        }

        // POST: ActivosFijos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Descripcion,DepartamentoID,Tipo_ActivoID,Fecha_Registro,Valor_Compra,Depreciacion_Acumulada")] ActivosFijos activosFijos)
        {
            if (id != activosFijos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(activosFijos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivosFijosExists(activosFijos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartamentoID"] = new SelectList(_context.Set<Departamento>(), "Id", "Descripcion", activosFijos.DepartamentoID);
            ViewData["Tipo_ActivoID"] = new SelectList(_context.Set<TipoDeActivo>(), "Id", "Descripcion", activosFijos.Tipo_ActivoID);
            return View(activosFijos);
        }

        // GET: ActivosFijos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activosFijos = await _context.ActivosFijos
                .Include(a => a.Departamento)
                .Include(a => a.Tipo_Activo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (activosFijos == null)
            {
                return NotFound();
            }

            return View(activosFijos);
        }

        // POST: ActivosFijos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var activosFijos = await _context.ActivosFijos.FindAsync(id);
            _context.ActivosFijos.Remove(activosFijos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivosFijosExists(int id)
        {
            return _context.ActivosFijos.Any(e => e.Id == id);
        }
    }
}
