﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Models
{
    public class TipoDeActivo
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        public string Descripcion { get; set; }

        [Display(Name = "Cuenta contable de compra")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Cuenta_Contable_Compra { get; set; }

        [Display(Name = "Cuenta contable de depreciacion")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Cuenta_Contable_Depreciacion { get; set; }

        public bool Estado { get; set; }

        public ICollection<ActivosFijos> ActivosFijos { get; set; }
    }
}
